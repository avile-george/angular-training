import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Test1Component } from './test/test1.component';
import { HomeComponent } from './home/home.component';
import { ProductModule } from './products/product.module';
import { CustomerComponent } from './customer/customer.component';
import { CustomerReactComponent } from './customer/customer-react.component';

@NgModule({
  declarations: [
    AppComponent,
    Test1Component,
    HomeComponent,
    CustomerComponent,
    CustomerReactComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ProductModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
