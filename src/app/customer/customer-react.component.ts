import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl } from '@angular/forms';
import { Customer } from './customer';

@Component({
  selector: 'app-customer-react',
  templateUrl: './customer-react.component.html',
  styleUrls: ['./customer-react.component.css']
})
export class CustomerReactComponent implements OnInit {

  customerForm: FormGroup;
  customer = new Customer();

  constructor() { }

  ngOnInit() {
    this.customerForm = new FormGroup({
      fName: new FormControl(),
      lName: new FormControl(),
      email: new FormControl()
    });
  }

  save() {
    console.log(`Saved : ${JSON.stringify(this.customerForm.value)}`);
  }

}
