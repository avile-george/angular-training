import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';



@Component({
  selector: 'app-star',
  templateUrl: './star.component.html',
  styleUrls: ['./star.component.css']
})
export class StarComponent implements OnInit, OnChanges {
  @Input() rating: number;
  starWidth: number;
  @Output() ratingClicked: EventEmitter<string> = new EventEmitter<string>();

  constructor() {
  }

  ngOnInit() {
    this.starWidth = this.rating * 75 / 5;
  }

  ngOnChanges() {
    // this.starWidth = this.rating * 75 / 5;
  }

  starClick() {
    this.ratingClicked.emit(`This rating ${this.rating} was clicked`);
  }
}
