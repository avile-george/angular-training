import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test1',
  template: `
    <h2 class="header">
      test1 works!
    </h2>
    <p>{{title}}</p>
    <button class="btn btn-primary"> Click!
    </button>
  `,
  styles: [`.header { color: red}`]
})
export class Test1Component implements OnInit {
  title = 'Learning one way binding..';
  constructor() { }

  ngOnInit() {
  }

}
