import { Pipe, PipeTransform } from '@angular/core';
import { IProduct } from './product';

@Pipe({
  name: 'productFilter'
})
export class ProductFilterPipe implements PipeTransform {

  transform(value: IProduct[], filterText: string): IProduct[] {
    filterText = filterText ? filterText.toLocaleLowerCase() : null;

    const filtered = value.filter(p => !filterText || p.productName.toLocaleLowerCase().includes(filterText));

    return filtered.length > 0 ? filtered : value;
  }

}
