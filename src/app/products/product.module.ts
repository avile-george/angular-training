import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ProductListComponent } from './product-list.component';
import { ProductDetailsGuard } from './product-details.guard';
import { ProductDetailsComponent } from './product-details.component';
import { ProductFilterPipe } from './product-filter.pipe';
import { ProductCodeFilterPipe } from './product-code-filter.pipe';
import { StarComponent } from '../shared/star.component';
import { SharedModule } from '../shared/shared.module';



@NgModule({
  declarations: [
    ProductListComponent,
    ProductDetailsComponent,
    ProductFilterPipe,
    ProductCodeFilterPipe
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild([{ path: 'products', component: ProductListComponent },
    { path: 'products/:id', component: ProductDetailsComponent, canActivate: [ProductDetailsGuard] }]),
    SharedModule
  ],

})
export class ProductModule { }
