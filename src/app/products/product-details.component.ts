import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IProduct } from './product';
import { ProductService } from './product.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {
  pageTitle = 'Product Detail';
  product: IProduct;

  constructor(private router: ActivatedRoute, private productService: ProductService) { }

  ngOnInit() {
    const id = +this.router.snapshot.paramMap.get('id');
    if (id) {
      this.pageTitle += ` : ${id}`;
      this.productService.getProductById(id).subscribe(product => this.product = product);
    }
  }

}
