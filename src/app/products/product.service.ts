import { Injectable } from '@angular/core';
import { IProduct } from './product';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { tap, catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class ProductService {
  constructor(private httpClient: HttpClient) { }

  getProducts(): Observable<IProduct[]> {
    return this.httpClient.get<IProduct[]>('assets/api/products/products.json')
      .pipe(
        tap(x => console.log(`All products data : ${JSON.stringify(x)}`)),
        catchError(this.handleError)
      );
  }

  getProductById(id: number): Observable<IProduct> {
    return this.getProducts()
      .pipe(
        map((products: IProduct[]) =>
          products.find(p => p.productId === id)),
          catchError(this.handleError)
      );
  }

  private handleError(err: HttpErrorResponse) {
    let msg = '';
    if (err.error instanceof ErrorEvent) {
      msg = `An error occured ${err.error}`;
    } else {
      msg = `Server returned code : ${err.status}, message: ${err.message}`;
    }
    return throwError(msg);
  }
}
