import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'productCodeFilter'
})
export class ProductCodeFilterPipe implements PipeTransform {

  transform(value: string): any {
    return value ? value.replace('-', ' ') : value;
  }

}
