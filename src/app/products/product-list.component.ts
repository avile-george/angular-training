import { Component, OnInit } from '@angular/core';
import { IProduct } from './product';
import { ProductService } from './product.service';

@Component({
     selector: 'app-product-list',
     templateUrl: './product-list.component.html',
     styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
     pageTitle = 'Product - List';
     filterText = '';
     showImage = true;
     imageWidth = 50;
     imageHeight = 50;
     products: IProduct[];
     errorMessage = '';

     constructor(private productService: ProductService) {
     }

     ngOnInit() {
          this.productService.getProducts().subscribe(
               (data: IProduct[]) => this.products = data,
               error => this.errorMessage = error
          );
     }

     toggleImage(): void {
          this.showImage = !this.showImage;
     }

     onRatingClicked(msg: string): void {
          this.pageTitle = `Product list - ${msg}`;
     }
}
